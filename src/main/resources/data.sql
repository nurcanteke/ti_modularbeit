insert into answer(id, option1, option2, option3, selected, answer)
values (1, 'Die Theorie der Computersprachen', 'Die Wissenschaft von Computern', 'Keine', -1, 2);

insert into answer(id, option1, option2, option3, selected, answer)
values (2, 'Programmiersprache', 'Webanwendung zur Versionsverwaltung', 'Content-Management-System', -1, 1);

insert into answer(id, option1, option2, option3, selected, answer)
values (3, 'Stellen die zentralen Organisationseinheiten eines Qualltexts dar', 'Mit Java Klassen werden Gestaltungsanweisungen erstellt', 'Beide Antworten sind richtig', -1, 1);

insert into answer(id, option1, option2, option3, selected, answer)
values (4, 'American special computer language in informatics', 'American standard code for information interchange', 'Nichts von den beiden', -1, 2);

insert into answer(id, option1, option2, option3, selected, answer)
values (5, 'if-else', 'for', 'when', -1, 2);

insert into question(id, text, score, answer_id)
values (1, 'Was ist Informatik?', 0, 1);
insert into question(id, text, score, answer_id)
values (2, 'Was ist Java?', 0, 2);
insert into question(id, text, score, answer_id)
values (3, 'Was beschreibt eine Klasse in Java?', 0, 3);
insert into question(id, text, score, answer_id)
values (4, 'Für was steht ASCII?', 0, 4);
values (4, 'Mit Welchem Keyword werden Bedingungen in java geschrieben?', 0, 4);
