package ti_modularbeit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ti_modularbeit.entity.Question;
import ti_modularbeit.repo.QuestionRepo;


/**
 * diese klasse wirkt bietet methoden die dem controller bedienen.
 * Diese klasse verarbeitet die klassen aus der DB und gibt sie weiter an den Controller.
 * @author nurcan
 */
@Service
public class QuestionService
{

  // inject the db repo
  @Autowired
  QuestionRepo questionRepo;

  /**
   * Gibt alle Fragen zurück, die in der datenbank vorhanden sind.
   *
   * @return alle fragen.
   */
  public List<Question> getAllQuestions()
  {
    //Get all the questions
    List<Question> questions = questionRepo.findAll();
    //Create an array for sorting
    Question[] arr = new Question[questions.size()];
    Question[] questionArray = questions.toArray(arr);
    //Bubble sort algorithm
    //Two for loops for going through the list
    for(int i = 0;i < questions.size();i++){
      for(int j = 0; j < questions.size()-1;j++){
        Question one = questionArray[j];
        Question two = questionArray[j+1];
        //Swap if two bigger than one
        if(!(one.getScore() >= two.getScore())){
          questionArray[j] = two;
          questionArray[j+1] = one;
        }
      }
    }
    //Put back into list
    questions.clear();
    for(int i = 0; i < questionArray.length;i++){
      questions.add(questionArray[i]);
    }
    return questions;

    //Alternatively
    //return questionRepo.findAll(Sort.by(Sort.Direction.DESC, "score"));
  }

  /**
   * Gibt eine frage zurück, die zu einem gegebenen id gehört.
   *
   * @param id id der frage, die zu finden ist.
   */
  public Question getById(int id)
  {
    Optional<Question> q = questionRepo.findById(id);
    if (q.isPresent())
    {
      return q.get();
    }
    else
    {
      throw new RuntimeException("Question not found");
    }
  }

  /**
   * speichert eine gegebene frage in die datenbank.
   *
   * @param question frage objekt
   */
  public void saveQuestion(Question question)
  {
    questionRepo.save(question);
  }
}
