package ti_modularbeit.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


/**
 * fragen entity und erstellt die fragen tabelle in der h2 datenbank.
 *
 * @author nurcan
 */
@Entity
public class Question
{

  // automatische generierung
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String text;

  private int score;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "answer_id", foreignKey = @ForeignKey(name = "fk_question_answer"))
  private Answer answer;

  public int getId()
  {
    return id;
  }

  public void setId(final int id)
  {
    this.id = id;
  }

  public String getText()
  {
    return text;
  }

  public void setText(final String text)
  {
    this.text = text;
  }

  public int getScore()
  {
    return score;
  }

  public void setScore(final int score)
  {
    this.score = score;
  }

  public Answer getAnswer()
  {
    return answer;
  }

  public void setAnswer(final Answer answer)
  {
    this.answer = answer;
  }
}
