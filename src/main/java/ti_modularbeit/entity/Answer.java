package ti_modularbeit.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * entity klasse für die antwort einer frage
 * @author nurcan
 */
@Entity
public class Answer
{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String option1;

  private String option2;

  private String option3;

  private int selected;

  private int answer;

  public int getId()
  {
    return id;
  }

  public void setId(final int id)
  {
    this.id = id;
  }

  public String getOption1()
  {
    return option1;
  }

  public void setOption1(final String option1)
  {
    this.option1 = option1;
  }

  public String getOption2()
  {
    return option2;
  }

  public void setOption2(final String option2)
  {
    this.option2 = option2;
  }

  public String getOption3()
  {
    return option3;
  }

  public void setOption3(final String option3)
  {
    this.option3 = option3;
  }

  public int getSelected()
  {
    return selected;
  }

  public void setSelected(final int selected)
  {
    this.selected = selected;
  }

  public int getAnswer()
  {
    return answer;
  }

  public void setAnswer(final int answer)
  {
    this.answer = answer;
  }
}
