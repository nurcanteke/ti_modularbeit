package ti_modularbeit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ti_modularbeit.entity.Question;
import ti_modularbeit.service.QuestionService;


/**
 * Der Hauptcontroller, der die requests aus den views empfängt und die daten bereitstellt.
 *
 * @author nurcan
 */
@Controller
public class MainController
{

  // inject von fragen service
  @Autowired
  QuestionService questionService;

  /**
   * Diese Methode wird vom view questions benutzt, gibt alle fragen an das view zurück und gibt diese übersicht zurück.
   *
   * @param model spring model zum speichern von daten
   * @return
   */
  @GetMapping(path = "/")
  public String getAllQuestions(Model model)
  {
    List<Question> questions = questionService.getAllQuestions();
    model.addAttribute("questions", questions);
    return "questions.html";
  }

  /**
   * diese Methode wird beim öffnen der übersicht einer frage (question.html) eine frage wird aus dem fragen service abgefragt und an die übersicht weitergegeben.
   *
   * @param id die frage id
   * @param m model zur speicherung und weitergabe von daten an die views
   * @return gibt das view der frage zurück
   */
  @GetMapping("/updateQuestion/{id}")
  public String eineFrage(@PathVariable int id, Model m)
  {
    Question question = questionService.getById(id);
    m.addAttribute("question", question);
    return "question.html";
  }

  /**
   * Diese methode wird aufgerufen nachdem eine frage beantwortet wurde und die antwort submittet werden soll.
   *
   * @param question frage die aus der formular kommt
   * @param ra speicherung von attributen, die an eine andere Übersucht weitergegeben werden sollen.
   * @return die übersicht zu dem weitergeleitet wird.
   */
  @PostMapping("/submitAnswer")
  public String submitAnswer(@ModelAttribute("question") Question question, RedirectAttributes ra)
  {
    Question questionToUpdate = questionService.getById(question.getId());
    if (question.getAnswer()
                .getSelected() == question.getAnswer()
                                          .getAnswer())
    {
      questionToUpdate.setScore(questionToUpdate.getScore() + 1);
      questionService.saveQuestion(questionToUpdate);
      ra.addFlashAttribute("successMessage", "Die Antwort war richtig");
    }
    else
    {
      ra.addFlashAttribute("wrongMessage", "Die Antwort war falsch");
    }
    return "redirect:/";
  }
}
