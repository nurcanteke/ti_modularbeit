package ti_modularbeit.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ti_modularbeit.entity.Question;


/**
 * beschreibt die persistence schicht und wird benutzt um daten aus der db zu erhalten und dahin zu speichern.
 * @author nurcan
 */
@Repository
public interface QuestionRepo extends JpaRepository<Question, Integer>
{

}
